package com.aem.sling.core;

import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

@Component(immediate=true, label="ANDIE Service", description="Hello There � This is a Service component", configurationFactory=true, metatype=true)
@Service({SampleService.class})
public class SampleServiceImpl
        implements SampleService
{
    @Property(description="This is Label for Sampleservice")
    private static final String SERVICE_LABEL = "service.label";
    @Property(description="This is Config input to test", value={"Hello There � this is property value"})
    static final String SERVICE_VARIABLE = "service.variable";
    private String serviceVariable;

    public String getData()
    {
        return "Return data from Service:" + this.serviceVariable;
    }

    @Activate
    protected void activate(ComponentContext ctx)
    {
        this.serviceVariable = PropertiesUtil.toString(ctx.getProperties().get("service.variable"), "service.variable");
    }

    @Modified
    protected void modified(ComponentContext ctx)
    {
        this.serviceVariable = PropertiesUtil.toString(ctx.getProperties().get("service.variable"), "service.variable");
    }
}
