package com.aem.sling.core.servlets;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.IOException;

@SlingServlet(paths={"/bin/newServ"}, methods={"GET"}, resourceTypes={"compCount/components/content/counter"})
public class counterServlet
        extends SlingAllMethodsServlet
{
    private static final Logger LOGGER = LoggerFactory.getLogger(counterServlet.class);
    @Reference
    ResourceResolverFactory resourceResolverFactory;
    ResourceResolver resourceResolver;
    static Long nodeval = null;

    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException
    {
        try
        {
            String resourcePath = "/content/counter";
            response.setHeader("content-type", "text/html");
            this.resourceResolver = this.resourceResolverFactory.getAdministrativeResourceResolver(null);
            LOGGER.info("After Resource Resolver");
            Resource res = this.resourceResolver.getResource(resourcePath);
            String path = res.getPath();
            Node newNode = (Node)res.adaptTo(Node.class);
            if (!newNode.hasProperty("nodeCounter"))
            {
                newNode.setProperty("nodeCounter",1);
            }
            else
            {
                nodeval = Long.valueOf(newNode.getProperty("nodeCounter").getLong());
                newNode.setProperty("nodeCounter",nodeval+1);
            }
            Session session = (Session)this.resourceResolver.adaptTo(Session.class);
            session.save();
            session.logout();
        }
        catch (LoginException e)
        {
            e.printStackTrace();
        }
        catch (RepositoryException e)
        {
            e.printStackTrace();
        }
    }

}
