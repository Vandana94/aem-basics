package com.aem.sling.core;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class SlingBean {
    @Inject
    private String favCartoon;
    @Inject
    private String favName;
    @Inject
    private String favDesc;
    @Inject
    private String favImage;

    public String getFavCartoon() {
        return favCartoon;
    }

    public void setFavCartoon(String favCartoon) {
        this.favCartoon = favCartoon;
    }

    public String getFavName() {
        return favName;
    }

    public void setFavName(String favName) {
        this.favName = favName;
    }

    public String getFavDesc() {
        return favDesc;
    }

    public void setFavDesc(String favDesc) {
        this.favDesc = favDesc;
    }

    public String getFavImage() {
        return favImage;
    }

    public void setFavImage(String favImage) {
        this.favImage = favImage;
    }
}
