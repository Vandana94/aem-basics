package com.aem.sling.core.servlets;

import com.aem.sling.core.SampleService;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.IOException;

@SlingServlet(paths={"/services/aemvardhan/callservice"}, methods={"GET"})
public class SampleServlet
        extends SlingAllMethodsServlet
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SampleServlet.class);
    @Reference
    SampleService varService;

    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        LOGGER.info("Value of varService.getData():: " + this.varService.getData());
        response.getWriter().write(this.varService.getData());
    }

    protected void bindVarService(SampleService paramSampleService)
    {
        this.varService = paramSampleService;
    }

    protected void unbindVarService(SampleService paramSampleService)
    {
        if (this.varService == paramSampleService) {
            this.varService = null;
        }
    }
}
