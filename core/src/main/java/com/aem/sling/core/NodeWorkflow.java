package com.aem.sling.core;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Component(immediate=true, enabled=true, metatype=true)
@Service({WorkflowProcess.class})
@org.apache.felix.scr.annotations.Property(name="process.label", value={"Create Child Node and Update"}, propertyPrivate=true)
public class NodeWorkflow
        implements WorkflowProcess
{
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeWorkflow.class);
    @Reference
    ResourceResolverFactory resourceResolverFactory;
    ResourceResolver resourceResolver;

    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException
    {
        WorkflowData workflowData = workItem.getWorkflowData();
        String type = workflowData.getPayloadType();
        LOGGER.info("PayLoad Type:: " + type);
        LOGGER.info("WorkFlow Data:: " + workflowData);

        Map<String, Value> parentMap = new HashMap();

        LOGGER.info("After Resource Resolver");
        if (!StringUtils.equals(type, "JCR_PATH")) {
            return;
        }
        Session session = workflowSession.getSession();
        String path = workflowData.getPayload().toString();
        try
        {
            Node node = session.getNode(path);
            LOGGER.info("Node Name is" + node.getName());

            PropertyIterator parentProps = node.getProperties();
            while (parentProps.hasNext())
            {
                javax.jcr.Property eachProp = parentProps.nextProperty();
                LOGGER.info("Property Name:: " + eachProp.getName() + "Property Value" + eachProp.getValue());
                if (!eachProp.getName().equalsIgnoreCase("jcr:primaryType")) {
                    parentMap.put(eachProp.getName(), eachProp.getValue());
                }
            }
            Node childnode = session.getNode("/content/counterPage/jcr:content/par");
            LOGGER.info("Child Node name is: " + childnode.getName());

            Node newNode = childnode.addNode(node.getName(), "nt:unstructured");
            LOGGER.info("New Node Name is:: " + newNode.getName());

            Set entrySet = parentMap.entrySet();
            Iterator newprops = entrySet.iterator();
            while (newprops.hasNext())
            {
                Map.Entry me = (Map.Entry)newprops.next();
                newNode.setProperty((String)me.getKey(), (Value)me.getValue());
                LOGGER.info("Key is: " + me.getKey() + " &  value is: " + me

                        .getValue());
            }
        }

        

        catch (Exception ex)
        {
            LOGGER.info("Node WorkFlow Exception::" + ex.getMessage());
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
