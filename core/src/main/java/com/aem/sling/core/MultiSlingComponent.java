package com.aem.sling.core;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.Session;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables={Resource.class})
public class MultiSlingComponent
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiSlingComponent.class);
    SlingBean menuItem = null;
    @Inject
    @Named("favouriteSling")
    private String[] favouriteSling;
    List<SlingBean> favList = new ArrayList();
    @OSGiService
    ResourceResolverFactory resolverFactory;
    ResourceResolver resourceResolver;

    @PostConstruct
    public void init()
    {
        try
        {
            String resourcePath = "/content/favSling/favChild";
            ResourceResolver resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
            Resource res = resourceResolver.getResource(resourcePath);

            String path = res.getPath();
            Session session = (Session)resourceResolver.adaptTo(Session.class);
            LOGGER.info("After session node");

            Node node = session.getNode(resourcePath);
            LOGGER.info("After adding node");
            if (this.favouriteSling != null)
            {
                this.favList = new ArrayList();
                for (String favString : this.favouriteSling)
                {
                    JSONObject jsonObject = new JSONObject(favString);
                    LOGGER.info("json object is " + jsonObject);
                    this.menuItem = new SlingBean();
                    String name = jsonObject.getString("favName");
                    String desc = jsonObject.getString("favDesc");
                    String favCartoon = jsonObject.getString("favCartoon");
                    String favImage = jsonObject.getString("favImage");
                    this.menuItem.setFavName(name);
                    this.menuItem.setFavDesc(desc);
                    this.menuItem.setFavCartoon(favCartoon);
                    this.menuItem.setFavImage(favImage);
                    this.favList.add(this.menuItem);
                }
            }
            session.save();
            session.logout();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception while Multifield data {} " + e.getMessage(), e);
        }
    }

    public List<SlingBean> getList()
    {
        LOGGER.info("inside Getlist");
        return this.favList;
    }
}
